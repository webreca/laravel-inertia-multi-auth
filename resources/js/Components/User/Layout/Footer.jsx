import { Link } from '@inertiajs/react';

export default function Footer({ user }) {
    return (
        <footer className="footer-dark bg-dark py-2">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-6">
                        <img
                            alt=""
                            className="logo-dark"
                            height="18"
                            src="assets/images/logo.png"
                        />
                        <p className="text-muted mt-4">
                            Hyper makes it easier to build better websites with
                            <br />
                            {' '}great speed. Save hundreds of hours of design
                            <br />
                            {' '}and development by using it.
                        </p>
                        <ul className="social-list list-inline mt-3">
                            <li className="list-inline-item text-center">
                                <a
                                    className="social-list-item border-blue text-blue"
                                    href="#"
                                >
                                    <i className="mdi mdi-facebook" />
                                </a>
                            </li>
                            <li className="list-inline-item text-center">
                                <a
                                    className="social-list-item border-danger text-danger"
                                    href="#"
                                >
                                    <i className="mdi mdi-google" />
                                </a>
                            </li>
                            <li className="list-inline-item text-center">
                                <a
                                    className="social-list-item border-info text-info"
                                    href="#"
                                >
                                    <i className="mdi mdi-twitter" />
                                </a>
                            </li>
                            <li className="list-inline-item text-center">
                                <a
                                    className="social-list-item border-secondary text-secondary"
                                    href="#"
                                >
                                    <i className="mdi mdi-github" />
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="col-lg-2 mt-3 mt-lg-0">
                        <h5 className="text-light">
                            Company
                        </h5>
                        <ul className="list-unstyled ps-0 mb-0 mt-3">
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    About Us
                                </a>
                            </li>
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Documentation
                                </a>
                            </li>
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Blog
                                </a>
                            </li>
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Affiliate Program
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="col-lg-2 mt-3 mt-lg-0">
                        <h5 className="text-light">
                            Apps
                        </h5>
                        <ul className="list-unstyled ps-0 mb-0 mt-3">
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Ecommerce Pages
                                </a>
                            </li>
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Email
                                </a>
                            </li>
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Social Feed
                                </a>
                            </li>
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Projects
                                </a>
                            </li>
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Tasks Management
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="col-lg-2 mt-3 mt-lg-0">
                        <h5 className="text-light">
                            Discover
                        </h5>
                        <ul className="list-unstyled ps-0 mb-0 mt-3">
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Help Center
                                </a>
                            </li>
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Our Products
                                </a>
                            </li>
                            <li className="mt-2">
                                <a
                                    className="text-muted"
                                    href="#"
                                >
                                    Privacy
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="mt-5">
                            <p className="text-muted mt-4 text-center mb-0">
                                © 2024 - 2025 Hyper. Design and coded by N2R Technologies
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}
