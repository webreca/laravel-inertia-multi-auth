import { Link } from '@inertiajs/react';

export default function Navbar({user}) {
    return (
        <nav className="navbar navbar-expand-lg fixed-top py-lg-3 navbar-dark bg-dark">
        <div className="container-fluid">
            <Link
                className="navbar-brand me-lg-5"
                href="/"
            >
                <img
                    alt=""
                    className="logo-dark"
                    height="18"
                    src="/assets/images/logo.png"
                />
            </Link>

            <button
                aria-controls="navbarNavDropdown"
                aria-expanded="false"
                aria-label="Toggle navigation"
                className="navbar-toggler"
                data-bs-target="#navbarNavDropdown"
                data-bs-toggle="collapse"
                type="button"
            >
                <i className="mdi mdi-menu" />
            </button>
            <div
                className="collapse navbar-collapse me-lg-5"
                id="navbarNavDropdown"
            >
                <ul className="navbar-nav mx-auto align-items-center">
                    <li className="nav-item mx-lg-1">
                        <a
                            className="nav-link active"
                            href="#"
                        >
                            Home
                        </a>
                    </li>
                    <li className="nav-item mx-lg-1">
                        <a
                            className="nav-link"
                            href="#"
                        >
                            About Us
                        </a>
                    </li>
                    <li className="nav-item mx-lg-1">
                        <a
                            className="nav-link"
                            href="#"
                        >
                            FAQs
                        </a>
                    </li>
                    <li className="nav-item mx-lg-1">
                        <a
                            className="nav-link"
                            href="#"
                        >
                            Clients
                        </a>
                    </li>
                    <li className="nav-item mx-lg-1">
                        <a
                            className="nav-link"
                            href="#"
                        >
                            Contact
                        </a>
                    </li>
                </ul>
                <ul className="navbar-nav ms-auto align-items-center">
                    <li className="dropdown notification-list">
                    <a
                        aria-expanded="false"
                        aria-haspopup="false"
                        className="nav-link dropdown-toggle web-nav-user arrow-none me-0 bg-dark"
                        data-bs-toggle="dropdown"
                        href="#"
                        role="button"
                    >
                        <span className="account-user-avatar">
                            <img
                                alt="user-image"
                                className="rounded-circle"
                                src="/assets/images/users/avatar-1.jpg"
                            />
                        </span>
                        <span>
                            <span className="account-user-name">
                                {user.name}
                            </span>
                            <span className="account-position">
                                Customer
                            </span>
                        </span>
                    </a>
                    <div className="dropdown-menu dropdown-menu-end dropdown-menu-animated topbar-dropdown-menu profile-dropdown">
                        <div className=" dropdown-header noti-title">
                            <h5 className="text-overflow m-0">
                                Welcome {user.name}!
                            </h5>
                        </div>
                        <Link
                            className="dropdown-item notify-item"
                            href={route('profile.edit')}
                        >
                            <i className="uil-user-square text-primary me-1" style={{ fontSize:"20px" }}/>
                            <span>
                                My Profile
                            </span>
                        </Link>
                        <Link
                            className="dropdown-item notify-item"
                            method="post" href={route('logout')} as="button"
                        >
                            <i className="uil-exit text-primary me-1" style={{ fontSize:"20px" }}/>
                            <span>
                                Logout
                            </span>
                        </Link>
                    </div>
                </li>
                    </ul>
            </div>
        </div>
    </nav >
    )
}
