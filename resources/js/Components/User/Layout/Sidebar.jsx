import { Link } from '@inertiajs/react';

export default function Sidebar(props) {
    return (
        <div className="list-group list-group-sidebar">
            <Link href={route('dashboard')} className={props.active == "dashboard" ? "list-group-item list-group-item-action active" : "list-group-item list-group-item-action"}><i className="uil-window me-2"></i><span>Dashboard</span></Link>
            <Link href={route('profile.edit')} className={props.active == "profile" ? "list-group-item list-group-item-action active" : "list-group-item list-group-item-action"}><i className="dripicons-user me-2"></i><span>My Profile</span></Link>
            <Link href={route('logout')} className="list-group-item list-group-item-action" method="post" as="button"><i className="dripicons-exit me-2"></i><span>Logout</span></Link>
            {/* <a data-bs-toggle="collapse" href="#sidebarEcommerce" aria-expanded="false" aria-controls="sidebarEcommerce" className="list-group-item list-group-item-action collapsed">
                <i className="dripicons-gear me-2"></i>
                <span>Ecommerce </span>
                <span className="menu-arrow"></span>
            </a>
            <div className="collapse" id="sidebarEcommerce">
                <ul className="side-nav-second-level">
                    <li>
                        <a href="apps-ecommerce-products.html">Change Password</a>
                    </li>
                </ul>
            </div> */}
        </div>
    );
}
