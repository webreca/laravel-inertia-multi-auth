import { Link } from '@inertiajs/react';

export default function Navbar({user}) {
    return (
        <div className="navbar-custom">
            <ul className="list-unstyled topbar-menu float-end mb-0">
                <li className="dropdown notification-list">
                    <a
                        aria-expanded="false"
                        aria-haspopup="false"
                        className="nav-link dropdown-toggle nav-user arrow-none me-0"
                        data-bs-toggle="dropdown"
                        href="#"
                        role="button"
                    >
                        <span className="account-user-avatar">
                            <img
                                alt="user-image"
                                className="rounded-circle"
                                src="/assets/images/users/avatar-1.jpg"
                            />
                        </span>
                        <span>
                            <span className="account-user-name">
                                {user.name}
                            </span>
                            <span className="account-position">
                                Administrator
                            </span>
                        </span>
                    </a>
                    <div className="dropdown-menu dropdown-menu-end dropdown-menu-animated topbar-dropdown-menu profile-dropdown">
                        <div className=" dropdown-header noti-title">
                            <h5 className="text-overflow m-0">
                                Welcome !
                            </h5>
                        </div>
                        <Link
                            className="dropdown-item notify-item"
                            href={route('administrator.profile.edit')}
                        >
                            <i className="uil-user-square text-primary me-1" style={{ fontSize:"20px" }}/>
                            <span>
                                Profile
                            </span>
                        </Link>
                        <Link
                            className="dropdown-item notify-item"
                            method="post" href={route('administrator.logout')} as="button"
                        >
                            <i className="uil-exit text-primary me-1" style={{ fontSize:"20px" }}/>
                            <span>
                                Logout
                            </span>
                        </Link>
                    </div>
                </li>
            </ul>
            <button className="button-menu-mobile open-left">
                <i className="mdi mdi-menu" />
            </button>
        </div>
    )
}
