import { Link } from '@inertiajs/react';

export default function Sidebar(props) {
    return (
        <div className="leftside-menu">

            <a href="/administrator" className="logo text-center logo-light">
                <span className="logo-lg">
                    <img src="/assets/images/logo.png" alt="" height="16" />
                </span>
                <span className="logo-sm">
                    <img src="/assets/images/logo_sm.png" alt="" height="16" />
                </span>
            </a>


            <a href="/administrator" className="logo text-center logo-dark">
                <span className="logo-lg">
                    <img src="/assets/images/logo-dark.png" alt="" height="16" />
                </span>
                <span className="logo-sm">
                    <img src="/assets/images/logo_sm_dark.png" alt="" height="16" />
                </span>
            </a>

            <div className="h-100" id="leftside-menu-container" data-simplebar>
                <ul className="side-nav">
                    <li className="side-nav-title side-nav-item">Navigation</li>

                    <li className="side-nav-item">
                        <Link href={route('administrator.dashboard')} className="side-nav-link">
                            <i className="uil-home-alt"></i>
                            <span> Dashboard </span>
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    );
}
