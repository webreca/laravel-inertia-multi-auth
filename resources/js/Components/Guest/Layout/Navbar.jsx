import { Link } from '@inertiajs/react';

export default function Navbar({ user }) {

    return (
        <nav className="navbar navbar-expand-lg fixed-top py-lg-3 navbar-dark bg-dark">
            <div className="container-fluid">
                <Link
                    className="navbar-brand me-lg-5"
                    href="/"
                >
                    <img
                        alt=""
                        className="logo-dark"
                        height="18"
                        src="/assets/images/logo.png"
                    />
                </Link>
                <button
                    aria-controls="navbarNavDropdown"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                    className="navbar-toggler"
                    data-bs-target="#navbarNavDropdown"
                    data-bs-toggle="collapse"
                    type="button"
                >
                    <i className="mdi mdi-menu" />
                </button>
                <div
                    className="collapse navbar-collapse me-lg-5"
                    id="navbarNavDropdown"
                >
                    <ul className="navbar-nav mx-auto align-items-center">
                        <li className="nav-item mx-lg-1">
                            <a
                                className="nav-link active"
                                href="#"
                            >
                                Home
                            </a>
                        </li>
                        <li className="nav-item mx-lg-1">
                            <a
                                className="nav-link"
                                href="#"
                            >
                                About Us
                            </a>
                        </li>
                        <li className="nav-item mx-lg-1">
                            <a
                                className="nav-link"
                                href="#"
                            >
                                FAQs
                            </a>
                        </li>
                        <li className="nav-item mx-lg-1">
                            <a
                                className="nav-link"
                                href="#"
                            >
                                Clients
                            </a>
                        </li>
                        <li className="nav-item mx-lg-1">
                            <a
                                className="nav-link"
                                href="#"
                            >
                                Contact
                            </a>
                        </li>
                    </ul>
                    <ul className="navbar-nav ms-auto align-items-center">
                        <li className="nav-item me-0">
                            {user ? (
                                <> <Link className="nav-link d-lg-none"
                                    href={route('dashboard')}
                                    target="_blank"
                                >
                                    Dashboard
                                </Link>
                                    <Link
                                        className="btn btn-sm btn-light btn-rounded d-none d-lg-inline-flex"
                                        href={route('dashboard')}
                                        target="_blank"
                                    >
                                        <i className="mdi mdi-basket me-1" />
                                        Dashboard
                                    </Link></>
                            ) : (
                                <>
                                    <Link className="nav-link d-lg-none"
                                        href={route('login')}
                                        target="_blank"
                                    >
                                        Login
                                    </Link>
                                    <Link className="nav-link d-lg-none"
                                        href={route('register')}
                                        target="_blank"
                                    >
                                        Register
                                    </Link>
                                    <Link
                                        className="btn btn-sm btn-danger btn-rounded d-none d-lg-inline-flex me-1"
                                        href={route('login')}
                                        target="_blank"
                                    >
                                        <i className="mdi mdi-login me-1" />
                                        Login
                                    </Link>
                                    <Link
                                        className="btn btn-sm btn-light btn-rounded d-none d-lg-inline-flex"
                                        href={route('register')}
                                        target="_blank"
                                    >
                                        <i className="mdi mdi-basket me-1" />
                                        Register
                                    </Link>
                                </>
                            )}

                        </li>
                    </ul>
                </div>
            </div>
        </nav >
    )
}
