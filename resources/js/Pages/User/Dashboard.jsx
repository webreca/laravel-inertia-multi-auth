import Layout from '@/Pages/User/Layout';
import { Head } from '@inertiajs/react';

export default function Dashboard({ auth }) {
    return (
        <Layout
            user={auth.user}
            active={`dashboard`}
        >
            <Head>
                <title>Dashboard</title>
                <meta name="description" content="Your page description" />
            </Head>
            <div className="row">
                <div className="col-12">
                    <div className="page-title-box">
                        <div className="page-title-right mt-0 d-block">
                        </div>
                        <h4 className="page-title">Dashboard</h4>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <div className="card-body">
                            <h4>You're logged in!</h4>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
