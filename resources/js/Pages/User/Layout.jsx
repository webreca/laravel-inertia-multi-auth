import Footer from '@/Components/User/Layout/Footer';
import Navbar from '@/Components/User/Layout/Navbar';
import Sidebar from '@/Components/User/Layout/Sidebar';

export default function Layout({ user, active, children }) {
    return (
        <>
            <div className="wrapper">
                <Navbar user={user} />
                <div className="row">
                    <div className="row">
                        <div className="col-sm-3">
                            <Sidebar active={active} />
                        </div>
                        <div className="col-sm-9">
                            <div className="content-page-user">
                                {children}
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        </>
    );
}
