import Layout from '@/Pages/Guest/Layout';
import { Head, Link, useForm } from '@inertiajs/react';

export default function VerifyEmail({ auth, status }) {
    const { post, processing } = useForm({});

    const submit = (e) => {
        e.preventDefault();

        post(route('verification.send'));
    };

    return (
        <Layout user={auth.user}
        active={`home`}>
            <Head title="Email Verification" />
            <section className="hero-section">
                <h3 className="text-center text-white">Thanks for signing up!</h3>
                <div className="row">
                    <div className="col-6 mx-auto">
                        <div className="card">
                            <div className="card-header text-center border-less">
                                {status === 'verification-link-sent' && (
                                    <h5 className='text-success'>
                                        A new verification link has been sent to the email address you provided during registration.
                                    </h5>
                                )}
                            </div>
                            <div className="card-body">
                                <div className="text-dark text-center">
                                    <p>Before getting started, could you verify your email address by clicking on the link we just emailed to you?</p>
                                    <p>If you didn't receive the email, we will gladly send you another.</p>
                                </div>
                            </div>
                            <div className="card-footer">
                                <form onSubmit={submit}>
                                    <div className="text-center flex items-center justify-between">
                                        <button type='submit' className='btn btn-sm btn-primary me-1' disabled={processing}>
                                            {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Resending Verification Email</> : <><i className="mdi mdi-send me-1" />Resend Verification Email</>}
                                        </button>
                                        <Link
                                            href={route('logout')}
                                            method="post"
                                            as="button"
                                            className="btn btn-sm btn-outline-dark"
                                        >
                                            <i className="mdi mdi-exit-run me-1" />Logout
                                        </Link>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    );
}
