import Layout from '@/Pages/Guest/Layout';
import InputError from '@/Components/InputError';
import { Head, Link, useForm } from '@inertiajs/react';

export default function ForgotPassword({ status }) {
    const { data, setData, post, processing, errors } = useForm({
        email: '',
    });

    const submit = (e) => {
        e.preventDefault();

        post(route('password.email'));
    };

    return (
        <Layout>
            <Head title="Forgot Password" />

            <section className='hero-section'>
                <h3 className="text-center text-white mt-5">Forgot your password?</h3>
                <div className="row">
                    <div className="col-4 mx-auto">
                        <div className="card">
                            <div className="card-body">
                                <p className="text-muted text-center mb-4">
                                    Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.
                                </p>
                                {status && <div className="mb-2 font-medium text-sm text-success">{status}</div>}
                                {/* form */}
                                <form onSubmit={submit}>
                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">
                                            Email address
                                        </label>
                                        <input
                                            className="form-control"
                                            type="email"
                                            id="email"
                                            name="email"
                                            value={data.email}
                                            placeholder="Enter your registered email address"
                                            isfocused="true"
                                            onChange={(e) => setData('email', e.target.value)}
                                        />
                                        <InputError message={errors.email} className="mt-1" />
                                    </div>
                                    <div className="mb-0 text-center d-grid">
                                        <button type='submit' className='btn btn-primary' disabled={processing}>
                                            {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Sending Reset Password Link</> : <><i className="mdi mdi-lock me-1" />Send Reset Password Link</>}
                                        </button>
                                        <p className="text-muted mt-2">
                                        Back to{" "}
                                        <Link href={route('login')} className="text-muted ms-1">
                                            <b>Log In</b>
                                        </Link>
                                    </p>
                                    </div>
                                </form>
                                {/* end form*/}

                            </div>{" "}
                            {/* end .card-body */}
                        </div>{" "}
                    </div>
                </div>
            </section>

        </Layout>
    );
}
