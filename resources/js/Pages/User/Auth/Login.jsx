import { useEffect } from 'react';
import Layout from '@/Pages/Guest/Layout';
import InputError from '@/Components/InputError';
import { Head, Link, useForm } from '@inertiajs/react';

export default function Login({ status, canResetPassword }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        email: '',
        password: '',
        remember: false,
    });

    useEffect(() => {
        return () => {
            reset('password');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('login'));
    };

    return (
        <Layout>
            <Head title="Log in" />
            <section className='hero-section'>
            <h3 className="text-center text-white mt-5">Login</h3>
                <div className="row">
                    <div className="col-4 mx-auto">
                        <div className="card">
                            <div className="card-body">
                                {/* title*/}
                                {status && <div className="mb-2 font-medium text-sm text-success">{status}</div>}
                                {/* form */}
                                <form onSubmit={submit}>
                                    <div className="mb-2">
                                        <label htmlFor="email" className="form-label">
                                            Email address
                                        </label>
                                        <input
                                            className="form-control"
                                            type="email"
                                            name="email"
                                            id="email"
                                            value={data.email}
                                            placeholder="Enter your email"
                                            autoComplete="username"
                                            isfocused="true"
                                            onChange={(e) => setData('email', e.target.value)}
                                        />
                                        <InputError message={errors.email} className="mt-1" />
                                    </div>
                                    <div className="mb-2">
                                        {canResetPassword && (
                                            <Link href={route('password.request')} className="text-muted float-end">
                                                <small>Forgot your password?</small>
                                            </Link>
                                        )}
                                        <label htmlFor="password" className="form-label">
                                            Password
                                        </label>
                                        <input
                                            className="form-control"
                                            type="password"
                                            name="password"
                                            id="password"
                                            autoComplete="current-password"
                                            onChange={(e) => setData('password', e.target.value)}
                                            placeholder="Enter your password"
                                        />
                                        <InputError message={errors.password} className="mt-1" />
                                    </div>
                                    <div className="mb-3">
                                        <div className="form-check">
                                            <input
                                                type="checkbox"
                                                className="form-check-input"
                                                id="remember"
                                                name='remember'
                                                checked={data.remember}
                                                onChange={(e) => setData('remember', e.target.checked)}
                                            />
                                            <label className="form-check-label" htmlFor="remember">
                                                Remember me
                                            </label>
                                        </div>
                                    </div>
                                    <div className="d-grid mb-0 text-center">
                                        <button type='submit' className='btn btn-primary' disabled={processing}>
                                            {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Logging in</> : <><i className="mdi mdi-login me-1" />Log In</>}
                                        </button>
                                    </div>
                                </form>
                                {/* end form*/}
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </Layout>
    );
}
