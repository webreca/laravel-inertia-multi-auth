import { useEffect } from 'react';
import Layout from '@/Pages/Guest/Layout/';
import InputError from '@/Components/InputError';
import { Head, Link, useForm } from '@inertiajs/react';

export default function ResetPassword({ status, token, email }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        token: token,
        email: email,
        password: '',
        password_confirmation: '',
    });

    useEffect(() => {
        return () => {
            reset('password', 'password_confirmation');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('password.store'));
    };

    return (
        <Layout>
            <Head title="Reset Password" />

            <section className='hero-section'>
                <h3 className="text-center text-white mt-5">Reset Password</h3>
                <div className="row">
                    <div className="col-4 mx-auto">
                        <div className="card">
                            <div className="card-body">
                                <p className="text-muted text-center mb-4">
                                    Just enter your desired new password & confirm it below.
                                </p>
                                {status && <div className="mb-2 font-medium text-sm text-success">{status}</div>}
                                {/* form */}
                                <form onSubmit={submit}>
                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">
                                            Email address
                                        </label>
                                        <input
                                            className="form-control"
                                            type="email"
                                            name="email"
                                            id="email"
                                            value={data.email}
                                            placeholder="Enter your email"
                                            autoComplete="username"
                                            isfocused="true"
                                            onChange={(e) => setData('email', e.target.value)}
                                            readOnly
                                        />
                                        <InputError message={errors.email} className="mt-1" />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label">
                                            New Password
                                        </label>
                                        <input
                                            className="form-control"
                                            type="password"
                                            name="password"
                                            id="password"
                                            autoComplete="current-password"
                                            onChange={(e) => setData('password', e.target.value)}
                                            placeholder="Enter your new password"
                                        />
                                        <InputError message={errors.password} className="mt-1" />
                                    </div>

                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label">
                                            Confirm Password
                                        </label>
                                        <input
                                            className="form-control"
                                            type="password"
                                            name="password_confirmation"
                                            id="password_confirmation"
                                            autoComplete="current-password"
                                            onChange={(e) => setData('password_confirmation', e.target.value)}
                                            placeholder="Re-enter your new password"
                                        />
                                        <InputError message={errors.password_confirmation} className="mt-1" />
                                    </div>

                                    <div className="d-grid mb-0 text-center">
                                        <button type='submit' className='btn btn-primary' disabled={processing}>
                                            {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Resetting Password</> : <><i className="mdi mdi-lock me-1" />Reset Password</>}
                                        </button>
                                        <p className="text-muted mt-2">
                                            Back to{" "}
                                            <Link href={route('login')} className="text-muted ms-1">
                                                <b>Log In</b>
                                            </Link>
                                        </p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    );
}
