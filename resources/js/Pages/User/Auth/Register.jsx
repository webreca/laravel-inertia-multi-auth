import { useEffect } from 'react';
import Layout from '@/Pages/Guest/Layout';
import InputError from '@/Components/InputError';
import { Head, Link, useForm } from '@inertiajs/react';

export default function Register() {
    const { data, setData, post, processing, errors, reset } = useForm({
        name: '',
        email: '',
        password: '',
        password_confirmation: '',
    });

    useEffect(() => {
        return () => {
            reset('password', 'password_confirmation');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('register'));
    };

    return (
        <Layout>
            <Head title="Register" />
            <section className='hero-section pt-3'>
                <h3 className="text-center text-white mt-5">Register</h3>
                <div className="row">
                    <div className="col-4 mx-auto">
                        <div className="card">
                            <div className="card-body">
                                {/* form */}
                                <form onSubmit={submit}>
                                    <div className="mb-2">
                                        <label htmlFor="name" className="form-label">
                                            Name
                                        </label>
                                        <input
                                            className="form-control"
                                            type="text"
                                            name="name"
                                            id="name"
                                            value={data.name}
                                            placeholder="Enter your name"
                                            autoComplete="name"
                                            isfocused="true"
                                            onChange={(e) => setData('name', e.target.value)}
                                        />
                                        <InputError message={errors.name} className="mt-1" />
                                    </div>
                                    <div className="mb-2">
                                        <label htmlFor="email" className="form-label">
                                            Email address
                                        </label>
                                        <input
                                            className="form-control"
                                            type="email"
                                            name="email"
                                            id="email"
                                            value={data.email}
                                            placeholder="Enter your email"
                                            autoComplete="username"
                                            onChange={(e) => setData('email', e.target.value)}
                                        />
                                        <InputError message={errors.email} className="mt-1" />
                                    </div>
                                    <div className="mb-2">
                                        <label htmlFor="password" className="form-label">
                                            Password
                                        </label>
                                        <input
                                            className="form-control"
                                            type="password"
                                            name="password"
                                            id="password"
                                            autoComplete="current-password"
                                            onChange={(e) => setData('password', e.target.value)}
                                            placeholder="Enter your password"
                                        />
                                        <InputError message={errors.password} className="mt-1" />
                                    </div>
                                    <div className="mb-2">
                                        <label htmlFor="password" className="form-label">
                                            Confirm Password
                                        </label>
                                        <input
                                            className="form-control"
                                            type="password"
                                            name="password_confirmation"
                                            id="password_confirmation"
                                            autoComplete="current-password"
                                            onChange={(e) => setData('password_confirmation', e.target.value)}
                                            placeholder="Confirm your password"
                                        />
                                        <InputError message={errors.password_confirmation} className="mt-1" />
                                    </div>
                                    <div className="d-grid mb-0 text-center">
                                        <button type='submit' className='btn btn-primary' disabled={processing}>
                                            {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Signing Up</> : <><i className="mdi mdi-login me-1" />Sign Up</>}
                                        </button>
                                        <p className="text-muted mt-2">
                                           Already Registered?
                                            <Link href={route('login')} className="text-muted ms-1">
                                                <b>Log In</b>
                                            </Link>
                                        </p>
                                    </div>
                                </form>
                                {/* end form*/}
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </Layout>
    );
}
