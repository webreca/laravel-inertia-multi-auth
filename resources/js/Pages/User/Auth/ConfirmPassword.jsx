import { useEffect } from 'react';
import Layout from '@/Pages/Guest/Layout/';
import InputError from '@/Components/InputError';
import { Head, Link, useForm } from '@inertiajs/react';

export default function ConfirmPassword({ status, token, email }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        token: token,
        email: email,
        password: '',
        password_confirmation: '',
    });

    useEffect(() => {
        return () => {
            reset('password');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('password.confirm'));
    };

    return (
        <Layout>
            <Head title="Reset Password" />

            <section className='hero-section'>
                <h3 className="text-center text-white mt-5">Confirm Password</h3>
                <div className="row">
                    <div className="col-4 mx-auto">
                        <div className="card">
                            <div className="card-body">
                                <p className="text-muted text-center mb-4">
                                    This is a secure area of the application. Please confirm your password before continuing.
                                </p>
                                {status && <div className="mb-2 font-medium text-sm text-success">{status}</div>}
                                {/* form */}
                                <form onSubmit={submit}>
                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label">
                                            Password
                                        </label>
                                        <input
                                            className="form-control"
                                            type="password"
                                            name="password"
                                            id="password"
                                            value={data.password}
                                            isFocused={true}
                                            onChange={(e) => setData('password', e.target.value)}
                                            placeholder="Confirm your password"
                                        />
                                        <InputError message={errors.password} className="mt-1" />
                                    </div>

                                    <div className="d-grid mb-0 text-center">
                                        <button type='submit' className='btn btn-primary' disabled={processing}>
                                            {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Confirming Password</> : <><i className="mdi mdi-lock me-1" />Confirm Password</>}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    );
}
