import Layout from '@/Pages/Admin/Layout';
import { Head } from '@inertiajs/react';

export default function Dashboard({ auth }) {
    return (
        <Layout
            user={auth.user}
            active={`dashboard`}
        >
            <Head title="Dashboard" />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="page-title-box">
                            <div className="page-title-right">
                            </div>
                            <h4 className="page-title">Dashboard</h4>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <h4>You're logged in!</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
