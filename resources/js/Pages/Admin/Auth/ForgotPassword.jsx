import InputError from '@/Components/InputError';
import { Head, Link, useForm } from '@inertiajs/react';

export default function ForgotPassword({ status }) {
    const { data, setData, post, processing, errors } = useForm({
        email: '',
    });

    const submit = (e) => {
        e.preventDefault();

        post(route('administrator.password.email'));
    };

    return (
        <>
            <Head title="Forgot Password" />

            <div className="auth-fluid">
                {/*Auth fluid left content */}
                <div className="auth-fluid-form-box">
                    <div className="align-items-center d-flex h-100">
                        <div className="card-body">
                            {/* Logo */}
                            <div className="auth-brand text-center text-lg-start">
                                <a href="index.html" className="logo-dark">
                                    <span>
                                        <img src="/assets/images/logo-dark.png" alt="" height={18} />
                                    </span>
                                </a>
                                <a href="index.html" className="logo-light">
                                    <span>
                                        <img src="/assets/images/logo.png" alt="" height={18} />
                                    </span>
                                </a>
                            </div>
                            {/* title*/}
                            <h4 className="mt-0">Forgot your password?</h4>
                            <p className="text-muted mb-4">
                                Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.
                            </p>
                            {status && <div className="mb-2 font-medium text-sm text-success">{status}</div>}
                            {/* form */}
                            <form onSubmit={submit}>
                                <div className="mb-3">
                                    <label htmlFor="email" className="form-label">
                                        Email address
                                    </label>
                                    <input
                                        className="form-control"
                                        type="email"
                                        id="email"
                                        name="email"
                                        value={data.email}
                                        placeholder="Enter your registered email address"
                                        isfocused="true"
                                        onChange={(e) => setData('email', e.target.value)}
                                    />
                                    <InputError message={errors.email} className="mt-1" />
                                </div>
                                <div className="mb-0 text-center d-grid">
                                    <button type='submit' className='btn btn-primary' disabled={processing}>
                                        {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Sending Reset Password Link</> : <><i className="mdi mdi-lock me-1" />Send Reset Password Link</>}
                                    </button>
                                </div>
                            </form>
                            {/* end form*/}
                            {/* Footer*/}
                            <footer className="footer footer-alt">
                                <p className="text-muted">
                                    Back to{" "}
                                    <Link href={route('administrator.login')} className="text-muted ms-1">
                                        <b>Log In</b>
                                    </Link>
                                </p>
                            </footer>
                        </div>{" "}
                        {/* end .card-body */}
                    </div>{" "}
                    {/* end .align-items-center.d-flex.h-100*/}
                </div>
                {/* end auth-fluid-form-box*/}
                {/* Auth fluid right content */}
                <div className="auth-fluid-right text-center">
                    <div className="auth-user-testimonial">
                        <h2 className="mb-3">Forgot your password?</h2>
                        <p className="lead">
                            <i className="mdi mdi-format-quote-open" /> Life isn't about finding yourself. Life is about creating yourself. <i className="mdi mdi-format-quote-close" />
                        </p>
                        <p>- Hyper Admin User</p>
                    </div>{" "}
                    {/* end auth-user-testimonial*/}
                </div>
                {/* end Auth fluid right content */}
            </div>

        </>
    );
}
