import { useEffect } from 'react';
import InputError from '@/Components/InputError';
import { Head, Link, useForm } from '@inertiajs/react';

export default function ResetPassword({ status, token, email }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        token: token,
        email: email,
        password: '',
        password_confirmation: '',
    });

    useEffect(() => {
        return () => {
            reset('password', 'password_confirmation');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('administrator.password.store'));
    };

    return (
        <>
            <Head title="Reset Password" />

            <div className="auth-fluid">
                {/*Auth fluid left content */}
                <div className="auth-fluid-form-box">
                    <div className="align-items-center d-flex h-100">
                        <div className="card-body">
                            {/* Logo */}
                            <div className="auth-brand text-center text-lg-start">
                                <a href="/administrator/login" className="logo-dark">
                                    <span>
                                        <img src="/assets/images/logo-dark.png" alt="" height={18} />
                                    </span>
                                </a>
                                <a href="/administrator/login" className="logo-light">
                                    <span>
                                        <img src="/assets/images/logo.png" alt="" height={18} />
                                    </span>
                                </a>
                            </div>
                            {/* title*/}
                            <h4 className="mt-2">Reset Password</h4>
                            <p className="text-muted mb-4">
                                Just enter your desired new password & confirm it below.
                            </p>
                            {status && <div className="mb-2 font-medium text-sm text-success">{status}</div>}
                            {/* form */}
                            <form onSubmit={submit}>
                                <div className="mb-3">
                                    <label htmlFor="email" className="form-label">
                                        Email address
                                    </label>
                                    <input
                                        className="form-control"
                                        type="email"
                                        name="email"
                                        id="email"
                                        value={data.email}
                                        placeholder="Enter your email"
                                        autoComplete="username"
                                        isfocused="true"
                                        onChange={(e) => setData('email', e.target.value)}
                                        readOnly
                                    />
                                    <InputError message={errors.email} className="mt-1" />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="password" className="form-label">
                                        New Password
                                    </label>
                                    <input
                                        className="form-control"
                                        type="password"
                                        name="password"
                                        id="password"
                                        autoComplete="current-password"
                                        onChange={(e) => setData('password', e.target.value)}
                                        placeholder="Enter your new password"
                                    />
                                    <InputError message={errors.password} className="mt-1" />
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="password" className="form-label">
                                        Confirm Password
                                    </label>
                                    <input
                                        className="form-control"
                                        type="password"
                                        name="password_confirmation"
                                        id="password_confirmation"
                                        autoComplete="current-password"
                                        onChange={(e) => setData('password_confirmation', e.target.value)}
                                        placeholder="Re-enter your new password"
                                    />
                                    <InputError message={errors.password_confirmation} className="mt-1" />
                                </div>

                                <div className="d-grid mb-0 text-center">
                                    <button type='submit' className='btn btn-primary' disabled={processing}>
                                        {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Resetting Password</> : <><i className="mdi mdi-lock me-1" />Reset Password</>}
                                    </button>
                                </div>
                            </form>
                            {/* Footer*/}
                            <footer className="footer footer-alt">
                                <p className="text-muted">
                                    Back to{" "}
                                    <Link href={route('administrator.login')} className="text-muted ms-1">
                                        <b>Log In</b>
                                    </Link>
                                </p>
                            </footer>
                            {/* end form*/}
                        </div>
                        {/* end .card-body */}
                    </div>
                    {/* end .align-items-center.d-flex.h-100*/}
                </div>
                {/* end auth-fluid-form-box*/}
                {/* Auth fluid right content */}
                <div className="auth-fluid-right text-center">
                    <div className="auth-user-testimonial">
                        <h2 className="mb-3">Administrator Reset Password</h2>
                        <p className="lead">
                            <i className="mdi mdi-format-quote-open" /> Whatever we believe about ourselves and our ability comes true for us. <i className="mdi mdi-format-quote-close" />
                        </p>
                        <p>- Hyper Admin User</p>
                    </div>
                    {/* end auth-user-testimonial*/}
                </div>
                {/* end Auth fluid right content */}
            </div>
        </>
    );
}
