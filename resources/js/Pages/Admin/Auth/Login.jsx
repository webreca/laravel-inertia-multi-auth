import { useEffect } from 'react';
import InputError from '@/Components/InputError';
import { Head, Link, useForm } from '@inertiajs/react';

export default function Login({ status, canResetPassword }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        email: '',
        password: '',
        remember: false,
    });

    useEffect(() => {
        return () => {
            reset('password');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('administrator.login'));
    };

    return (
        <>
            <Head title="Log in" />

            <div className="auth-fluid">
                {/*Auth fluid left content */}
                <div className="auth-fluid-form-box">
                    <div className="align-items-center d-flex h-100">
                        <div className="card-body">
                            {/* Logo */}
                            <div className="auth-brand text-center text-lg-start">
                                <a href="/administrator/login" className="logo-dark">
                                    <span>
                                        <img src="/assets/images/logo-dark.png" alt="" height={18} />
                                    </span>
                                </a>
                                <a href="/administrator/login" className="logo-light">
                                    <span>
                                        <img src="/assets/images/logo.png" alt="" height={18} />
                                    </span>
                                </a>
                            </div>
                            {/* title*/}
                            <h4 className="mt-2">Administrator Login</h4>
                            <p className="text-muted mb-4">
                                Enter your email address & password to access account.
                            </p>
                            {status && <div className="mb-2 font-medium text-sm text-success">{status}</div>}
                            {/* form */}
                            <form onSubmit={submit}>
                                <div className="mb-3">
                                    <label htmlFor="email" className="form-label">
                                        Email address
                                    </label>
                                    <input
                                        className="form-control"
                                        type="email"
                                        name="email"
                                        id="email"
                                        value={data.email}
                                        placeholder="Enter your email"
                                        autoComplete="username"
                                        isfocused="true"
                                        onChange={(e) => setData('email', e.target.value)}
                                    />
                                    <InputError message={errors.email} className="mt-1" />
                                </div>
                                <div className="mb-3">
                                    {canResetPassword && (
                                        <Link href={route('administrator.password.request')} className="text-muted float-end">
                                            <small>Forgot your password?</small>
                                        </Link>
                                    )}
                                    <label htmlFor="password" className="form-label">
                                        Password
                                    </label>
                                    <input
                                        className="form-control"
                                        type="password"
                                        name="password"
                                        id="password"
                                        autoComplete="current-password"
                                        onChange={(e) => setData('password', e.target.value)}
                                        placeholder="Enter your password"
                                    />
                                    <InputError message={errors.password} className="mt-1" />
                                </div>
                                <div className="mb-3">
                                    <div className="form-check">
                                        <input
                                            type="checkbox"
                                            className="form-check-input"
                                            id="remember"
                                            name='remember'
                                            checked={data.remember}
                                            onChange={(e) => setData('remember', e.target.checked)}
                                        />
                                        <label className="form-check-label" htmlFor="remember">
                                            Remember me
                                        </label>
                                    </div>
                                </div>
                                <div className="d-grid mb-0 text-center">
                                    <button type='submit' className='btn btn-primary' disabled={processing}>
                                        {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Logging in</> : <><i className="mdi mdi-login me-1" />Log In</>}
                                    </button>
                                </div>
                            </form>
                            {/* end form*/}
                        </div>
                        {/* end .card-body */}
                    </div>
                    {/* end .align-items-center.d-flex.h-100*/}
                </div>
                {/* end auth-fluid-form-box*/}
                {/* Auth fluid right content */}
                <div className="auth-fluid-right text-center">
                    <div className="auth-user-testimonial">
                        <h2 className="mb-3">Administrator Login</h2>
                        <p className="lead">
                            <i className="mdi mdi-format-quote-open" /> Whatever we believe about ourselves and our ability comes true for us. <i className="mdi mdi-format-quote-close" />
                        </p>
                        <p>- Hyper Admin User</p>
                    </div>
                    {/* end auth-user-testimonial*/}
                </div>
                {/* end Auth fluid right content */}
            </div>
            </>
    );
}
