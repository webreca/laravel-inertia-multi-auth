import { useState } from 'react';
import Sidebar from '@/Components/Admin/Layout/Sidebar';
import Navbar from '@/Components/Admin/Layout/Navbar';

export default function Layout({ user, active, children }) {

    return (
        <div className="wrapper">
            <Sidebar active={active}/>
            <div className="content-page">
                <div className="content">
                    <Navbar user={user}/>
                    {children}
                </div>
            </div>
        </div>
    );
}
