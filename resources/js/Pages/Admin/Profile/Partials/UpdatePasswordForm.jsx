import { useRef } from 'react';
import InputError from '@/Components/InputError';
import { useForm } from '@inertiajs/react';
import { Transition } from '@headlessui/react';

export default function UpdatePasswordForm({ className = '' }) {
    const passwordInput = useRef();
    const currentPasswordInput = useRef();

    const { data, setData, errors, put, reset, processing, recentlySuccessful } = useForm({
        current_password: '',
        password: '',
        password_confirmation: '',
    });

    const updatePassword = (e) => {
        e.preventDefault();

        put(route('administrator.password.update'), {
            preserveScroll: true,
            onSuccess: () => reset(),
            onError: (errors) => {
                if (errors.password) {
                    reset('password', 'password_confirmation');
                    passwordInput.current.focus();
                }

                if (errors.current_password) {
                    reset('current_password');
                    currentPasswordInput.current.focus();
                }
            },
        });
    };

    return (
        <div className="card">
            <div className="card-header pb-0">
                <h5 className="card-title text-primary mb-1">Update Password</h5>
                <p>
                    Ensure your account is using a long, random password to stay secure.
                </p>
                <Transition
                    show={recentlySuccessful}
                    enter="transition ease-in-out"
                    enterFrom="opacity-0"
                    leave="transition ease-in-out"
                    leaveTo="opacity-0"
                >
                    <p className="text-sm text-success">Password updated successfully.</p>
                </Transition>
            </div>
            <div className="card-body">

                <form onSubmit={updatePassword}>
                <div className="mb-2 row">
                        <label htmlFor="current_password" className="col-sm-2 col-form-label col-form-label-sm">Current Password</label>
                        <div className="col-sm-10">
                            <input
                                className="form-control"
                                type="password"
                                ref={currentPasswordInput}
                                value={data.current_password}
                                onChange={(e) => setData('current_password', e.target.value)}
                                id="current_password"
                                placeholder="Enter your current password"
                                autoComplete="current-password"
                            />
                            <InputError message={errors.current_password} className="mb-0" />
                        </div>
                    </div>

                    <div className="mb-2 row">
                        <label htmlFor="password" className="col-sm-2 col-form-label col-form-label-sm">New Password</label>
                        <div className="col-sm-10">
                            <input
                                className="form-control"
                                type="password"
                                ref={passwordInput}
                                value={data.password}
                                onChange={(e) => setData('password', e.target.value)}
                                id="password"
                                placeholder="Enter your new password"
                                autoComplete="new-password"
                            />
                            <InputError message={errors.password} className="mb-0" />
                        </div>
                    </div>

                    <div className="mb-2 row">
                        <label htmlFor="password_confirmation" className="col-sm-2 col-form-label col-form-label-sm">Confirm Password</label>
                        <div className="col-sm-10">
                            <input
                                className="form-control"
                                type="password"
                                value={data.password_confirmation}
                                onChange={(e) => setData('password_confirmation', e.target.value)}
                                id="password_confirmation"
                                placeholder="Re-enter your new password"
                                autoComplete="new-password"
                            />
                            <InputError message={errors.password_confirmation} className="mb-0" />
                        </div>
                    </div>

                    <div className="text-end">
                        <button type='submit' className='btn btn-primary' disabled={processing}>
                            {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Updating</> : <><i className="uil-lock-alt me-1" />Update</>}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}
