import InputError from '@/Components/InputError';
import { Link, useForm, usePage } from '@inertiajs/react';
import { Transition } from '@headlessui/react';

export default function UpdateProfileInformation({ mustVerifyEmail, status, className = '' }) {
    const user = usePage().props.auth.user;

    const { data, setData, patch, errors, processing, recentlySuccessful } = useForm({
        name: user.name,
        email: user.email,
    });

    const submit = (e) => {
        e.preventDefault();

        patch(route('administrator.profile.update'));
    };

    return (
        <div className="card">
            <div className="card-header pb-0">
                <h5 className="card-title text-primary mb-1">Profile Information</h5>
                <p>
                    Update your account's profile information and email address.
                </p>
                <Transition
                    show={recentlySuccessful}
                    enter="transition ease-in-out"
                    enterFrom="opacity-0"
                    leave="transition ease-in-out"
                    leaveTo="opacity-0"
                >
                    <p className="text-sm text-success">Profile updated successfully.</p>
                </Transition>
            </div>
            <div className="card-body">
                <form onSubmit={submit}>
                    <div className="mb-2 row">
                        <label htmlFor="name" className="col-sm-2 col-form-label col-form-label-sm">Name</label>
                        <div className="col-sm-10">
                            <input
                                className="form-control"
                                type="text"
                                name="name"
                                id="name"
                                value={data.name}
                                placeholder="Enter your name"
                                autoComplete="name"
                                onChange={(e) => setData('name', e.target.value)}
                            />
                            <InputError message={errors.name} className="mb-0" />
                        </div>
                    </div>

                    <div className="mb-2 row">
                        <label htmlFor="email" className="col-sm-2 col-form-label col-form-label-sm">Email</label>
                        <div className="col-sm-10">
                            <input
                                className="form-control"
                                type="email"
                                name="email"
                                id="email"
                                value={data.email}
                                placeholder="Enter your email"
                                autoComplete="username"
                                onChange={(e) => setData('email', e.target.value)}
                            />
                            <InputError message={errors.email} className="mb-0" />
                        </div>
                    </div>

                    <div className="mb-2 row">
                        <label htmlFor="email" className="col-sm-2 col-form-label col-form-label-sm">Email Verified</label>
                        <div className="col-sm-10">
                            {mustVerifyEmail && user.email_verified_at === null ? (
                                <div className="input-group">
                                    <input type="text" className="form-control text-danger" value="Your email address is unverified." readOnly />
                                    <Link
                                        href={route('verification.send')}
                                        method="post"
                                        as="button"
                                        className="btn btn-outline-danger"
                                    >
                                        Click here to re-send the verification email.
                                    </Link>
                                </div>
                            ) : (
                                <div className="input-group">
                                    <input type="text" className="form-control text-success" value="Your email address is verified." readOnly />
                                </div>
                            )}
                        </div>
                    </div>

                    <div className="text-end">
                        <button type='submit' className='btn btn-primary' disabled={processing}>
                            {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Saving</> : <><i className="uil-database me-1" />Save</>}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}
