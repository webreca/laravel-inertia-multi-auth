import { useRef, useState } from 'react';
import InputError from '@/Components/InputError';
import { useForm } from '@inertiajs/react';

export default function DeleteUserForm({ className = '' }) {
    const [confirmingUserDeletion, setConfirmingUserDeletion] = useState(false);
    const passwordInput = useRef();

    const {
        data,
        setData,
        delete: destroy,
        processing,
        reset,
        errors,
    } = useForm({
        password: '',
    });

    const confirmUserDeletion = () => {
        setConfirmingUserDeletion(true);
    };

    const deleteUser = (e) => {
        e.preventDefault();

        destroy(route('administrator.profile.destroy'), {
            preserveScroll: true,
            onSuccess: () => closeModal(),
            onError: () => passwordInput.current.focus(),
            onFinish: () => reset(),
        });
    };

    const closeModal = () => {
        setConfirmingUserDeletion(false);

        reset();
    };

    return (
        <div className="card">
            <div className="card-header pb-0">
                <h5 className="card-title text-primary mb-1">Delete Account</h5>
                <p>
                    Once your account is deleted, all of its resources and data will be permanently deleted. Before
                    deleting your account, please download any data or information that you wish to retain.
                </p>
            </div>
            <div className="card-body">
                <div className="text-end">
                    <button type='button' onClick={confirmUserDeletion} className='btn btn-danger' disabled={processing}>
                        {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Deleting Account</> : <><i className="uil-trash-alt me-1" />Delete Account</>}
                    </button>
                </div>
            </div>
            <div>
                <div
                    aria-hidden="true"
                    className={confirmingUserDeletion ? "modal fade show" : "modal fade"} style={{ display: confirmingUserDeletion ? "block" : "none" }}
                    id="alert-modal"
                    role="dialog"
                    tabIndex="-1"
                >
                    <div className="modal-dialog modal-md border">
                        <div className="modal-content">
                            <div className="modal-header bg-danger">
                                <h4 class="modal-title text-white" id="standard-modalLabel">Are you sure you want to delete your account?</h4>
                                <button type="button" class="btn-close text-white" onClick={closeModal}></button>
                            </div>
                            <div className="modal-body">
                                <form onSubmit={deleteUser}>

                                    <p className="mt-1 text-sm text-dark">
                                        Once your account is deleted, all of its resources and data will be permanently deleted. Please
                                        enter your password to confirm you would like to permanently delete your account.
                                    </p>

                                    <div className="mb-2 row">
                                        <label htmlFor="password" className="col-sm-3 col-form-label col-form-label-sm">Password</label>
                                        <div className="col-sm-9">
                                            <input
                                                className="form-control"
                                                type="password"
                                                ref={passwordInput}
                                                value={data.password}
                                                onChange={(e) => setData('password', e.target.value)}
                                                id="password"
                                                isFocused
                                                placeholder="Enter your password"
                                                autoComplete="current-password"
                                            />
                                            <InputError message={errors.password} className="mb-0" />
                                        </div>
                                    </div>

                                   <div className="mb-2 row">
                                   <label className="col-sm-3 col-form-label col-form-label-sm text-white">Action</label>
                                    <div className="col-sm-9 text-end">
                                        <button type='button' className='btn btn-sm btn-light me-1' onClick={closeModal}>Close</button>
                                        <button type='submit' className='btn btn-sm btn-danger' disabled={processing}>
                                        {processing ? <><i className="mdi mdi-spin mdi-loading me-1" />Deleting Account</> : <><i className="uil-trash-alt me-1" />Delete Account</>}
                                    </button>
                                    </div>
                                   </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}
