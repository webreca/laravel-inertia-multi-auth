import Layout from '@/Pages/Admin/Layout';
import { Head } from '@inertiajs/react';
import DeleteUserForm from './Partials/DeleteUserForm';
import UpdatePasswordForm from './Partials/UpdatePasswordForm';
import UpdateProfileInformationForm from './Partials/UpdateProfileInformationForm';

export default function Show({ auth, mustVerifyEmail, status }) {
    return (
        <Layout
            user={auth.user}
            active={`profile`}
        >
            <Head title="Profile" />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="page-title-box">
                            <div className="page-title-right">
                            </div>
                            <h4 className="page-title">Profile</h4>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <UpdateProfileInformationForm
                            mustVerifyEmail={mustVerifyEmail}
                            status={status}
                        />
                        <UpdatePasswordForm />
                        <DeleteUserForm />
                    </div>
                </div>
            </div>
        </Layout>
    );
}
