import Footer from '@/Components/Guest/Layout/Footer';
import Navbar from '@/Components/Guest/Layout/Navbar';
import { Link } from '@inertiajs/react';

export default function Layout({ user, children }) {
    return (
        <>
            <Navbar user={user}/>
            {children}
            <Footer />
        </>
    );
}
