import { Link, Head } from '@inertiajs/react';
import Layout from '@/Pages/Guest/Layout';
export default function Welcome({ auth, laravelVersion, phpVersion }) {

    return (
        <>
            <Layout
                user={auth.user}
                active={`home`}
            >
                <Head>
                    <title>Welcome</title>
                    <meta name="description" content="Your page description" />
                </Head>
                <section className="hero-section">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-md-5">
                                <div className="mt-md-4">
                                    <div>
                                        <span className="badge bg-danger rounded-pill">
                                            New
                                        </span>
                                        <span className="text-white-50 ms-1">
                                            This boilerplate is for official use only
                                        </span>
                                    </div>
                                    <h3 className="text-white fw-normal mb-4 mt-3 hero-title">
                                        Laravel 11 + Breeze + React (Inertia.js)
                                    </h3>
                                    <p className="mb-4 font-16 text-white-50">
                                        Hyper is a fully featured dashboard and admin template comes with tones of well designed UI elements, components, widgets and pages.
                                    </p>
                                    <a
                                        className="btn btn-success"
                                        href="#"
                                        target="_blank"
                                    >
                                        Preview{' '}
                                        <i className="mdi mdi-arrow-right ms-1" />
                                    </a>
                                </div>
                            </div>
                            <div className="col-md-5 offset-md-2">
                                <div className="text-md-end mt-3 mt-md-0">
                                    <img
                                        alt=""
                                        className="img-fluid"
                                        src="/assets/images/startup.svg"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    )
}
