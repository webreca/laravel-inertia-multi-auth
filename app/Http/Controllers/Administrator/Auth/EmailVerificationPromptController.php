<?php

namespace App\Http\Controllers\Administrator\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        return $request->user('administrator')->hasVerifiedEmail()
            ? redirect()->intended('/administrator')
            : Inertia::render('Auth/Admin/VerifyEmail', ['status' => session('status')]);
    }
}
