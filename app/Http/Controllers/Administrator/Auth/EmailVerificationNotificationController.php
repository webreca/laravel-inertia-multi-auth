<?php

namespace App\Http\Controllers\Administrator\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmailVerificationNotificationController extends Controller
{
    /**
     * Send a new email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if ($request->user('administrator')->hasVerifiedEmail()) {
            return redirect()->intended('/administrator');
        }

        $request->user('administrator')->sendEmailVerificationNotification();

        return back()->with('status', 'verification-link-sent');
    }
}
